#!/bin/bash
#SBATCH --ntasks=1 # 1 core(CPU)
#SBATCH --nodes=1 # Use 1 node
#SBATCH --job-name=RMDL # sensible name for the job
#SBATCH --mem=40G #Default memory per CPU is 3GB.
#SBATCH --partition=hugemem # Use the verysmallmem-partition for jobs requiring < 10 GB RAM.
#SBATCH --mail-user=oystein.monsen@nmbu.no # Email me when job is done.
#SBATCH --mail-type=ALL
#SBATCH --constraint=avx2


module load Anaconda3
source activate jalview
module load MAFFT/7.470-GCC-9.3.0-with-extensions
module load BLAST+/2.10.1-gompi-2020a
module load Perl/5.30.2-GCCcore-9.3.0

CONS=enrichedTEs.fa
REF=GCF_000233375.1_ICSASG_v2_genomic.fna
​
# remove characters that should not be in the headers
awk '{print$1;}' $CONS | sed 's/\//_/g' > $CONS.clean
​
# create BLAST database of the genome
makeblastdb -in $REF -out $REF -dbtype nucl -parse_seqids
​
# run the RModeler_pipeline.pl script
perl RMDL_curation_pipeline.pl $REF $REF $CONS.clean
​
# remove temporary files
rm *emp.out
​
# create output folder
mkdir final
​
# reshape files
cd aligned; for i in $(ls *.fa); do name=`ls $i | cut -f1 -d "."`; cat $i | perl -ne 'chomp;s/>\s+/>/;if(/>(\S+)/){$id{$1}++;$id2=$1;}if($id{$id2}==1){print "$_\n"}' >../final/$name.fa; done; cd ../
​
# remove alignents that are too "gappy"
cd final; for i in $(ls *.fa); do name=`ls $i | cut -f1 -d "."`; t_coffee -other_pg seq_reformat -in $i -action +rm_gap 95 >$name.gaps95.fa; done; cd ../


---
title: "Telomer_Distance_bins"
author: "oymo"
date: "12/17/2021"
output: html_document
---

```{r setup, include=FALSE}
library(data.table)
library(GenomicRanges)

chrtrf<-fread("/mnt/SCRATCH/oymo/SIMON_TE_ANNOT/repmodel2/simon_repmask/trf/simonchr_align.gff")
simonindels<-fread("/mnt/SCRATCH/kristenl/SV_TE/eu_merged_iris_no_simon.bed")
simonins<-fread("/mnt/SCRATCH/oymo/kristenblod/all_ins_dist.txt")
degeneratetrf<-fread("/mnt/SCRATCH/oymo/SIMON_TE_ANNOT/repmodel2/simon_repmask/trf/simonchr_merged.gff")
satannots<-fread("/mnt/users/oymo/SIMON_ANNOT_INFO/rm_annot/chrom/Simon_Final2021_CHR.fasta.ori.out")
coords<-fread("/mnt/users/oymo/kristina_samarbeid/Simon_telomere_bins.csv", sep = ",")
colnames(coords)[1:3]<-c("chrom", "start", "stop")
gr_coords<-makeGRangesFromDataFrame(coords, keep.extra.columns = TRUE)

TEs<-fread("/mnt/users/oymo/SIMON_ANNOT_INFO/rm_annot/chrom/tes_mask/Simon_TEs.gff", fill=TRUE)
TEs[,c("V1", "V2", "V3", "V4","V8", "V9", "V11", "V12", "V13", "V14", "V16"):=NULL]
colnames(TEs)<-c("chrom", "start", "end", "TEfam", "ID")
TEs[,tmp:=1]
TEs[,ID:=cumsum(tmp)]
TEs[,tmp:=NULL]
TEs[,tag:=str_extract(TEfam, ".+(?=_)")]


```

## R Markdown

This is an R Markdown document. Markdown is a simple formatting syntax for authoring HTML, PDF, and MS Word documents. For more details on using R Markdown see <http://rmarkdown.rstudio.com>.

When you click the **Knit** button a document will be generated that includes both content as well as the output of any embedded R code chunks within the document. You can embed an R code chunk like this:

```{r dels}
library(data.table)
library(GenomicRanges)

#satannots<-satannots[,.(V5, V6, V7)]
varcat<-"DEL"
repcat<-"SAT"
#colnames(satannots)<-c("chrom", "start", "end")
#setkey(satannots, chrom, start, end)

simondels<-simonindels[V4=="DEL" | V4=="INS"]

simondels[,V4:=NULL]
colnames(simondels)<-c("chrom", "start", "end")

setkey(simondels, chrom, start, end)

gr_b<-chrtrf[,c("V1", "V5", "V6")]
#gr_b<-satannots
colnames(gr_b)<-c("chrom", "start", "end")
setkey(gr_b, chrom, start)

gr_b <- makeGRangesFromDataFrame(gr_b)

gr_b<-GenomicRanges::reduce(gr_b)

gr_a <- makeGRangesFromDataFrame(simondels, keep.extra.columns = T)

gr_a <- GenomicRanges::reduce(gr_a)

pairs <- findOverlapPairs(gr_a, gr_b, ignore.strand = TRUE)
he<-gr_a[gr_a %over% gr_b,]

gr_none<-gr_a[!gr_a %over% gr_b,]

ans <- pairs

mcols(ans)$overlap_width <- width(pintersect(ans, ignore.strand = TRUE))

table<-as.data.table(ans)

table[,overlap_proportion:=overlap_width/first.width]

othertable<-as.data.table(gr_none)

colnames(othertable)<-c("first.seqnames", "first.start", "first.end", "first.width", "first.strand")

gr_table<-makeGRangesFromDataFrame(table[,.("chrom"=first.seqnames, "start"=first.start, "stop"=second.end)])
setkey(table, first.seqnames, first.start, first.end)
setkey(othertable, first.seqnames, first.start, first.end)

hm<-rbind(table, othertable, fill=TRUE)

hm[is.na(hm)]<-0

setkey(hm, first.seqnames, first.start, first.end)

inclnonoverlap<-hm

inclnonoverlap<-inclnonoverlap[,.(first.seqnames, first.start, first.end, first.width, second.start, second.end, overlap_width, overlap_proportion)]

overlaps<-GenomicRanges::reduce(overlaps)
bpoverlaps<-as.data.table(findOverlapPairs(gr_coords, overlaps))[,.("chrom"=first.X.seqnames, "start"= first.X.start, "end"=first.X.end, "status"=first.Status, "width"=second.width, "arm"=first.Chr_arm, "distance"=first.X.Distance_to_telomere, "Closest_telomere_end"=first.X.Closest_telomere_end, "arrayend"=second.end)]
bpoverlaps[,distance:=round(distance, digits = -6)]
bpoverlaps<-bpoverlaps[!(Closest_telomere_end>start & Closest_telomere_end<end)]


bpoverlaps[,distbp:=sum(width), by=distance]

bpoverplots<-unique(bpoverlaps[,.(chrom, start, end, distance, status, distbp)])
bpoverplots[,binno:= .N, by=distance][,propbin:=distbp/binno]

coords[,overlap_counts:=conte]
coords[,distance:=round(Distance_to_telomere, digits = -6)]
coords<-coords[!(Closest_telomere_end>start & Closest_telomere_end<stop)]
```



LAG PLOT
```{r fig, echo=FALSE}
my_colours <- RColorBrewer::brewer.pal(6, "Dark2")

curcoords<-coords[Status=="Current"]
histcoords<-coords[Status=="Historic"]

coords[,binno:= .N, by=distance][,distoverlap:=sum(overlap_counts)/binno, by=distance]
curcoords[,binno:= .N, by=distance][,distoverlap:=sum(overlap_counts)/binno, by=distance]
histcoords[,binno:= .N, by=distance][,distoverlap:=sum(overlap_counts)/binno, by=distance]

plotall<-unique(coords[,.(distance, distoverlap)])
plotcur<-unique(curcoords[,.(distance, distoverlap)])
plothist<-unique(histcoords[,.(distance, distoverlap)])
setkey(plotcur, distance)
setkey(plothist, distance)


ggplot(coords, mapping=aes(x=distance)) + 
  geom_col(aes(y=overlap_counts, fill=Status)) + theme(axis.text.x = element_text(size=12)) +
  #geom_line(aes(y=teprop, colour="TEs")) + theme(axis.text.x = element_text(size=12)) +
  scale_fill_manual(name = "", values = c("Historic" = my_colours[1], "Current" = my_colours[3])) +
  #scale_color_manual(name = "", values = c("TEs" = my_colours[2])) +
  #scale_y_continuous(breaks=seq(-0.03,0.03,by=0.01), labels=abs(seq(-0.03,0.03,by=0.01))) +    
  #scale_x_continuous(breaks=c(0, 100, 200, 300), labels=c("0Mb", "50Mb", "100Mb", "150Mb")) +
  #xlab("Position") +
  #ylab("Proportion per 0,5Mb")# +    
  theme(strip.text.y = element_text(face = "bold"),
        axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1),
        legend.title = element_blank(),
        legend.text = element_text(size=14),
        legend.spacing.y = unit(0.001, units="mm"),
        plot.title = element_text(hjust = 0.5, size =22))# -> DA_PLOT_ZONE

ggsave(DA_PLOT_ZONE, file ="/mnt/users/oymo/kristina_samarbeid/figurar/ssa01_TR_TE_indel.png", 
       width = 20, 
       height = 15,
       units = "cm")

ggplot(bpoverplots, mapping=aes(x=distance)) + 
  geom_col(aes(y=propbin, fill=status)) + theme(axis.text.x = element_text(size=12)) +
  #geom_line(aes(y=teprop, colour="TEs")) + theme(axis.text.x = element_text(size=12)) +
  scale_fill_manual(name = "", values = c("Historic" = my_colours[1], "Current" = my_colours[3])) +
  #scale_color_manual(name = "", values = c("TEs" = my_colours[2])) +
  #scale_y_continuous(breaks=seq(-0.03,0.03,by=0.01), labels=abs(seq(-0.03,0.03,by=0.01))) +    
  #scale_x_continuous(breaks=c(0, 100, 200, 300), labels=c("0Mb", "50Mb", "100Mb", "150Mb")) +
  #xlab("Position") +
  #ylab("Proportion per 0,5Mb")# +    
  theme(strip.text.y = element_text(face = "bold"),
        axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1),
        legend.title = element_blank(),
        legend.text = element_text(size=14),
        legend.spacing.y = unit(0.001, units="mm"),
        plot.title = element_text(hjust = 0.5, size =22))# -> DA_PLOT_ZONE

```
---
title: "Array_size_analysis"
author: "oymo"
date: "10/8/2021"
output: html_document
---

```{r setup, include=FALSE}
library(data.table)
library(GenomicRanges)
library(stringr)
```

## R Markdown

This is an R Markdown document. Markdown is a simple formatting syntax for authoring HTML, PDF, and MS Word documents. For more details on using R Markdown see <http://rmarkdown.rstudio.com>.

When you click the **Knit** button a document will be generated that includes both content as well as the output of any embedded R code chunks within the document. You can embed an R code chunk like this:

```{r lodding}
chrtrf<-fread("/mnt/SCRATCH/oymo/SIMON_TE_ANNOT/repmodel2/simon_repmask/trf/simonchr_align.gff")
simonindels<-fread("/mnt/SCRATCH/kristenl/SV_TE/eu_merged_iris_no_simon.bed")


simondels<-simonindels[V4=="DEL"]
simondels[,V4:=NULL]
colnames(simondels)<-c("chrom", "start", "end")

setkey(simondels, chrom, start, end)

gr_b<-chrtrf[,c("V1", "V5", "V6")]
colnames(gr_b)<-c("chrom", "start", "end")

setkey(gr_b, chrom, start)

gr_b <- makeGRangesFromDataFrame(gr_b, keep.extra.columns = TRUE)

gr_b<-GenomicRanges::reduce(gr_b)

gr_b<-as.data.table(gr_b)

gr_b[,no:=1]

gr_b[,ID:=cumsum(no)]

gr_b[,no:=NULL]

dt<-gr_b
gr_b <- makeGRangesFromDataFrame(gr_b, keep.extra.columns = TRUE)


gr_a <- makeGRangesFromDataFrame(simondels)

gr_a <- GenomicRanges::reduce(gr_a)

dt$overlaps <-countOverlaps(gr_b, gr_a)


cor(dt$overlaps, dt$width, method = "pearson")
cor(dt$overlaps, dt$width, method = "spearman")


```

## processing to arrays

processing to arrays, then counting SV overlaps

```{r pressure, echo=FALSE}

#FINN GC-GEHALT
chrtrf[,gc:=(str_count(chrtrf$V3, pattern="[CG]")/str_length(chrtrf$V3))]
chrtrf[,arraygc:=gc*(V6-V5)]
setkey(chrtrf, V1, V5, V6)
#fwrite(chrtrf[,.(V1, V5, V6, gc, arraygc)], file = "gccalc.gff", sep = "\t", col.names = FALSE)
gc_merged<-fread("/mnt/users/oymo/kristina_samarbeid/gcmerged.gff")
gc_merged[,corrGC:=V4/(V3-V2)]
colnames(gc_merged)<-c("chrom", "start", "stop", "GCbases", "corrGC")
gr_c<-makeGRangesFromDataFrame(gc_merged, keep.extra.columns = TRUE)

sv_overlap<-as.data.table(subsetByOverlaps(gr_c, gr_a, ignore.strand = TRUE))
non_sv_overlap<-as.data.table(subsetByOverlaps(gr_c, gr_a, ignore.strand = TRUE, invert = TRUE))
cor.test(sv_overlap$corrGC, sv_overlap$width, method = "pearson")
cor.test(non_sv_overlap$corrGC, non_sv_overlap$width, method = "pearson")

cor.test(sv_overlap$corrGC, sv_overlap$width, method = "spearman")
cor.test(non_sv_overlap$corrGC, non_sv_overlap$width, method = "spearman")

```

Note that the `echo = FALSE` parameter was added to the code chunk to prevent printing of the R code that generated the plot.

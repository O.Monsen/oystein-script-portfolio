

# write TE names to pdf
library(tidyverse)

tag <- read_tsv('data/translation_table/tag_translation.tsv')
tag <- as.data.frame(tag)
pdf('figure1/figure1_TEsuperfamily_and_TAGs_text.pdf')
plot('', ylim=c(0,nrow(tag)), xlim=c(-5,100), axes=F, xlab='', ylab='')
sapply(1:nrow(tag), function(i) text(labels = tag[i,2], y = i, x = 0, pos=2, family='mono'))
sapply(1:nrow(tag), function(i) text(labels = tag[i,1], y = i, x = 0, pos=4, family='mono'))
dev.off()

